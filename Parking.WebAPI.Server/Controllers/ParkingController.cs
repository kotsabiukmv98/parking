﻿using Microsoft.AspNetCore.Mvc;
using Parking.WebAPI.Server.Models;
using static Parking.Core.Application.ParkingService;

namespace Parking.WebAPI.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        // GET: api/Parking
        [HttpGet]
        public ParkingInfo Get()
        {
            return new ParkingInfo()
            {
                Balance              = Instance().Balance,
                VehiclesCount        = Instance().VehiclesCount,
                Capacity             = Instance().Capacity,
                RevenueForLastMinute = Instance().GetRevenueForLastMinute(),
                TransactionsCount    = Instance().TransactionsCount
            };
        }
    }
}
