﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Parking.WebAPI.Server.Models;
using static Parking.Core.Application.ParkingService;

namespace Parking.WebAPI.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        // GET: api/Transactions
        [HttpGet]
        public IEnumerable<TransactionInfo> Get()
        {
            return Instance().Transactions.Select(t =>
                new TransactionInfo()
                {
                    VehicleId = t.VehicleId,
                    Amount =  t.Amount,
                    CreationTime = t.DateTime
                }
            );
        }

    }
}
