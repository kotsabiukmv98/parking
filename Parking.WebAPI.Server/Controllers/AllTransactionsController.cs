﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using static Parking.Core.Application.ParkingService;

namespace Parking.WebAPI.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AllTransactionsController : ControllerBase
    {
        // GET: api/AllTransactions
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return Instance().GetLoggedTransactions();
        }
    }
}
