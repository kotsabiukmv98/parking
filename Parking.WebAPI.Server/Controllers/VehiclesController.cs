﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing.Constraints;
using Parking.Core.Domain.Vehicles;
using Parking.WebAPI.Server.Models;
using static Parking.Core.Application.ParkingService;


namespace Parking.WebAPI.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        // GET: api/Vehicle
        [HttpGet]
        public IEnumerable<VehicleInfo> Get()
        {
            return Instance().Vehicles.Select(v =>
                new VehicleInfo()
                {
                    Id = v.Id,
                    Balance = v.Balance,
                    CreationTime = v.CreationDateTime,
                    Type = v.GetVehicleType()
                });
        }

        // GET: api/Vehicle/5
        [HttpGet("{id}", Name = "Get")]
        public bool Get(int id)
        {
            return Instance().HasVehicle(id);
        }

        // POST: api/Vehicle/5
        [HttpPost]
        public void Post([FromHeader] int vehicleType, [FromHeader] double balance)
        {
            if (balance < 0)
                return;

            var vehicle = CreateVehicle(vehicleType, balance);
            if (vehicle != null)
                Instance().AddVehicle(vehicle);
        }

        // PUT: api/Vehicle/5
        [HttpPut("{id}")]
        public void Put(int id, [FromHeader] double amount)
        {
            if (amount > 0 && Instance().HasVehicle(id))
                Instance().RefillVehicle(id, amount);
        }

        // DELETE: api/vehicles/5
        [HttpDelete("{id}")]
        public bool Delete(int id)
        {
            return Instance().RemoveVehicle(id);
        }

        private Vehicle CreateVehicle(int vehicleType, double balance)
        {
            switch (vehicleType)
            {
                case 1: return new PassengerCar(balance);
                case 2: return new CargoCar(balance);
                case 3: return new Bus(balance);
                case 4: return new Motorcycle(balance);
                default: return null;
            }
        }
    }
}
