﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Parking.WebAPI.Server.Models
{
    public class ParkingInfo
    {
        public int Capacity { get; set; }

        public double Balance { get; set; }

        public double RevenueForLastMinute { get; set; }

        public int VehiclesCount { get; set; }

        public int TransactionsCount { get; set; }
    }
}
