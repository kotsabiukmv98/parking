﻿using System;

namespace Parking.WebAPI.Server.Models
{
    public class VehicleInfo
    {
        public int Id { get; set; }

        public string Type{ get; set; }

        public double Balance { get; set; }

        public DateTime CreationTime{ get; set; }
    }
}
