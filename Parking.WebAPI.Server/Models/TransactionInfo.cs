﻿using System;

namespace Parking.WebAPI.Server.Models
{
    public class TransactionInfo
    {
        public int VehicleId { get; set; }

        public double Amount { get; set; }

        public DateTime CreationTime { get; set; }
    }
}