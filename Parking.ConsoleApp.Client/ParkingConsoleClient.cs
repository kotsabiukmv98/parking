﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using Newtonsoft.Json;
using Parking.ConsoleApp.Client.Models;
using Parking.UI;

namespace Parking.ConsoleApp.Client
{
    public class ParkingConsoleClient : IParkingActions
    {
        public void AddVehicle(int vehicleType, double balance)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create($"https://localhost:5001/api/vehicles");
            request.Headers["vehicleType"] = vehicleType.ToString();
            request.Headers["balance"] = balance.ToString();

            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            request.Method = "POST";

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
            }
        }

        private string ParkingInfoRequest()
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://localhost:5001/api/parking");
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                return reader.ReadToEnd();
            }
        }

        public double GetBalance()
        {
            var parkingInfoJson = ParkingInfoRequest();
            var parkingInfo = JsonConvert.DeserializeObject<ParkingInfo>(parkingInfoJson);
            
            return parkingInfo.Balance;
        }

        public int GetCapacity()
        {
            var parkingInfoJson = ParkingInfoRequest();
            var parkingInfo = JsonConvert.DeserializeObject<ParkingInfo>(parkingInfoJson);

            return parkingInfo.Capacity;
        }

        public IList<string> GetLoggedTransactions()
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://localhost:5001/api/AllTransactions");
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            var responseJson = "";
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                responseJson = reader.ReadToEnd();
            }
            return JsonConvert.DeserializeObject<IList<string>>(responseJson);
        }

        public double GetRevenueForLastMinute()
        {
            var parkingInfoJson = ParkingInfoRequest();
            var parkingInfo = JsonConvert.DeserializeObject<ParkingInfo>(parkingInfoJson);

            return parkingInfo.RevenueForLastMinute;
        }

        public IList<string> GetTransactions()
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://localhost:5001/api/Transactions");
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            var responseJson = "";
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                responseJson = reader.ReadToEnd();
            }
            var transactions = JsonConvert.DeserializeObject<IList<TransactionInfo>>(responseJson);
            return transactions.Select(t =>
                $"{t.VehicleId,-20}{t.Amount,-25}{t.CreationTime.ToString(),-30}").ToList();
        }

        public int GetTransactionsCount()
        {
            var parkingInfoJson = ParkingInfoRequest();
            var parkingInfo = JsonConvert.DeserializeObject<ParkingInfo>(parkingInfoJson);

            return parkingInfo.TransactionsCount;
        }

        public IList<string> GetVehicles()
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://localhost:5001/api/vehicles");
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            var responseJson = "";
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                responseJson = reader.ReadToEnd();
            }
            var vehicles = JsonConvert.DeserializeObject<IList<VehicleInfo>>(responseJson);
            return vehicles.Select(v =>
                $"{v.Id,-20}{v.Type,-25}{v.Balance,-20:F2}{v.CreationTime.ToString(),-30}").ToList(); ;
        }

        public int GetVehiclesCount()
        {
            var parkingInfoJson = ParkingInfoRequest();
            var parkingInfo = JsonConvert.DeserializeObject<ParkingInfo>(parkingInfoJson);

            return parkingInfo.VehiclesCount;
        }

        public bool HasVehicle(int vehicleId)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create($"https://localhost:5001/api/vehicles/{vehicleId}");
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            var responseJson = "";
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                responseJson = reader.ReadToEnd();
            }
            return JsonConvert.DeserializeObject<bool>(responseJson);
        }

        public bool IsParkingFull()
        {
            var parkingInfoJson = ParkingInfoRequest();
            var parkingInfo = JsonConvert.DeserializeObject<ParkingInfo>(parkingInfoJson);

            return parkingInfo.Capacity == parkingInfo.VehiclesCount;
        }

        public void RefillVehicle(int vehicleId, double amount)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create($"https://localhost:5001/api/vehicles/{vehicleId}");
            request.Headers["amount"] = amount.ToString();
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            request.Method = "PUT";

            using (HttpWebResponse response = (HttpWebResponse) request.GetResponse())
            {
            }
        }

        public bool RemoveVehicle(int vehicleId)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create($"https://localhost:5001/api/vehicles/{vehicleId}");
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            request.Method = "DELETE";

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                return JsonConvert.DeserializeObject<bool>(reader.ReadToEnd());
            }
        }
    }
} 