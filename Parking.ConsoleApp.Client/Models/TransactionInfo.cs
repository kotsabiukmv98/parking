﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Parking.ConsoleApp.Client.Models
{
    class TransactionInfo
    {
        public int VehicleId { get; set; }

        public double Amount { get; set; }

        public DateTime CreationTime { get; set; }
    }
}
