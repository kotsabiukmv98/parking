﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Parking.ConsoleApp.Client.Models
{
    class ParkingInfo
    {
        public int Capacity { get; set; }

        public double Balance { get; set; }

        public double RevenueForLastMinute { get; set; }

        public int VehiclesCount { get; set; }

        public int TransactionsCount { get; set; }
    }
}
