﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Parking.ConsoleApp.Client.Models
{
    class VehicleInfo
    {
        public int Id { get; set; }

        public string Type { get; set; }

        public double Balance { get; set; }

        public DateTime CreationTime { get; set; }
    }
}
