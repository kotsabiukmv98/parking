﻿using System;
using Parking.UI;

namespace Parking.ConsoleApp.Client
{
    class Program
    {
        static void Main()
        {
            var consoleUI = new ConsoleUI(new ParkingConsoleClient());
            while (true)
                consoleUI.PromptInput();
        }
    }
}
