﻿using System;
using Parking.UI;

namespace Parking.ConsoleApp
{
    class Program
    {
        static void Main()
        {
            var consoleUI = new ConsoleUI(new ParkingConsoleApp());
            while (true)
                consoleUI.PromptInput();
        }
    }
}
