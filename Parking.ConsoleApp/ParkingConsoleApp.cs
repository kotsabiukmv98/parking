﻿using System;
using System.Collections.Generic;
using System.Linq;
using Parking.Core.Domain;
using Parking.Core.Domain.Vehicles;
using Parking.UI;
using static Parking.Core.Application.ParkingService;

namespace Parking.ConsoleApp
{
    public class ParkingConsoleApp : IParkingActions
    {
        public void AddVehicle(int vehicleType, double balance)
        {
            var vehicle = CreateVehicle(vehicleType, balance);
            
            Instance().AddVehicle(vehicle);
        }

        public int GetCapacity() => Instance().Capacity;

        public IList<string> GetLoggedTransactions() => Instance().GetLoggedTransactions();

        public double GetBalance() => Instance().Balance;

        public double GetRevenueForLastMinute() => Instance().GetRevenueForLastMinute();

        public bool RemoveVehicle(int vehicleId) => Instance().RemoveVehicle(vehicleId);

        public IList<string> GetTransactions()
        {
            return Instance().Transactions.Select(t =>
                $"{t.VehicleId,-20}{t.Amount,-25}{t.DateTime.ToString(),-30}").ToList();
        }

        public int GetTransactionsCount() => Instance().TransactionsCount;

        public IList<string> GetVehicles()
        {
            return Instance().Vehicles.Select(v =>
                $"{v.Id,-20}{v.GetVehicleType(),-25}{v.Balance,-20:F2}{v.CreationDateTime.ToString(),-30}").ToList();
        }

        public int GetVehiclesCount() => Instance().VehiclesCount;

        public bool HasVehicle(int vehicleId) => Instance().HasVehicle(vehicleId);

        public bool IsParkingFull() => Instance().IsParkingFull();

        public void RefillVehicle(int vehicleId, double amount)
        {
            Instance().RefillVehicle(vehicleId, amount);
        }

        private Vehicle CreateVehicle(int vehicleType, double balance)
        {
            switch (vehicleType)
            {
                case 1: return new PassengerCar(balance);
                case 2: return new CargoCar(balance);
                case 3: return new Bus(balance);
                case 4: return new Motorcycle(balance);
                default: throw  new NotImplementedException("No such vehicle");
            }
        }
    }
}