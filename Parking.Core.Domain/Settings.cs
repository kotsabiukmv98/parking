﻿namespace Parking.Core.Domain
{
    /// <summary>
    /// Global settings
    /// </summary>
    public static class Settings
    {
        /// <summary>
        /// Default parking balance
        /// </summary>
        public static double ParkingBalance = 0;

        /// <summary>
        /// Default parking capacity
        /// </summary>
        public static int ParkingCapacity = 10;

        /// <summary>
        /// Default interval of payment in seconds
        /// </summary>
        public static int PaymentInterval = 5;

        /// <summary>
        /// Default fine coefficient
        /// </summary>
        public static double FineCoefficient = 2.5;

        /// <summary>
        /// Default rate for passenger car
        /// </summary>
        public static double PassengerCarRate = 2;

        /// <summary>
        /// Default rate for cargo car
        /// </summary>
        public static double CargoCarRate = 5;

        /// <summary>
        /// Default rate for bus
        /// </summary>
        public static double BusRate = 3.5;

        /// <summary>
        /// Default rate for mororcycle
        /// </summary>
        public static double MotorcycleRate = 1;
    }
}
