﻿namespace Parking.Core.Domain.Vehicles
{
    /// <summary>
    /// Class for motorcycle
    /// </summary>
    public class Motorcycle : Vehicle
    {
        /// <summary>
        /// Parking rate for motorcycle
        /// </summary>
        public override double Rate { get; protected set; } = Settings.MotorcycleRate;

        /// <summary>
        /// Motorcycle's constructor
        /// </summary>
        public Motorcycle(double balance = 0) : base(balance)
        {
        }

        /// <summary>
        /// Gets type of vehicle
        /// </summary>
        /// <returns>Type of vehicle</returns>
        public override string GetVehicleType() => "Мотоцикл";
    }
}
