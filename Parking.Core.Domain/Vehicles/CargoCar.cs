﻿using Parking.Core.Domain;

namespace Parking.Core.Domain.Vehicles
{
    /// <summary>
    /// Class for cargo car
    /// </summary>
    public class CargoCar : Vehicle
    {
        /// <summary>
        /// Parking rate for cargo car
        /// </summary>
        public override double Rate { get; protected set; } = Settings.CargoCarRate;

        /// <summary>
        /// Cargo car's constructor
        /// </summary>
        public CargoCar(double balance = 0) : base(balance)
        {
        }

        /// <summary>
        /// Gets type of vehicle
        /// </summary>
        /// <returns>Type of vehicle</returns>
        public override string GetVehicleType() => "Вантажний автомобіль";
    }
}
