﻿namespace Parking.Core.Domain.Vehicles
{
    /// <summary>
    /// Class for bus
    /// </summary>
    public class Bus : Vehicle
    {
        /// <summary>
        /// Parking rate for bus
        /// </summary>
        public override double Rate { get; protected set; } = Settings.BusRate;

        /// <summary>
        /// Bus's constructor
        /// </summary>
        public Bus(double balance = 0) : base(balance)
        {
        }

        /// <summary>
        /// Gets type of vehicle
        /// </summary>
        /// <returns>Type of vehicle</returns>
        public override string GetVehicleType() => "Автобус";
    }
}
