﻿namespace Parking.Core.Domain.Vehicles
{
    /// <summary>
    /// Class for passenger car
    /// </summary>
    public class PassengerCar : Vehicle
    {
        /// <summary>
        /// Parking rate for passenger car
        /// </summary>
        public override double Rate { get; protected set; } = Settings.PassengerCarRate;

        /// <summary>
        /// Passenger car's constructor
        /// </summary>
        public PassengerCar(double balance = 0) : base(balance)
        {
        }

        /// <summary>
        /// Gets type of vehicle
        /// </summary>
        /// <returns>Type of vehicle</returns>
        public override string GetVehicleType() => "Легкове авто";
    }
}
