﻿using System;
using System.Timers;

namespace Parking.Core.Domain.Vehicles
{
    /// <summary>
    /// Base class for all type of vehicles
    /// </summary>
    public abstract class Vehicle
    {
        private static int id = 0;

        /// <summary>
        /// Vehicle creation date and time
        /// </summary>
        public DateTime CreationDateTime { get; private set; }

        /// <summary>
        /// Vehicle's ID
        /// </summary>
        public int Id { get; private set; }

        /// <summary>
        /// Parking rate for vehicle
        /// </summary>
        public abstract double Rate { get; protected set; }

        /// <summary>
        /// Gets vehicle's concrete type
        /// </summary>
        /// <returns>Type of vehicle</returns>
        public abstract string GetVehicleType();

        /// <summary>
        /// Vehicle's balance
        /// </summary>
        public double Balance { get; protected set; }

        /// <summary>
        /// Timer for payment
        /// </summary>
        public Timer Payment = new Timer(Settings.PaymentInterval * 1000);

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="balance">Vehicles's balance</param>
        public Vehicle(double balance)
        {
            Id = id++;
            Balance = balance;
            CreationDateTime = DateTime.Now;
        }
        
        /// <summary>
        /// Vehicle's balance replenish
        /// </summary>
        /// <param name="amount">Amount of money for replenish</param>
        public void ReplenishBalance(double amount)
        {
            Balance += amount;
        }

        /// <summary>
        /// Vehicle's parking payment
        /// </summary>
        /// <param name="amount">Amount of money for payment</param>
        public void Pay(double amount)
        {
            Balance -= amount;
        }
    }
}
