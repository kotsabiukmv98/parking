﻿using System;

namespace Parking.Core.Domain
{
    /// <summary>
    /// Class that represent transaction
    /// </summary>
    public class Transaction
    {
        /// <summary>
        /// Vehicle's ID
        /// </summary>
        public int VehicleId { get; set; }

        /// <summary>
        /// Creation date and time
        /// </summary>
        public DateTime DateTime { get; set; }

        /// <summary>
        /// Amount of money in transaction
        /// </summary>
        public double Amount { get; set; }
    }
}
