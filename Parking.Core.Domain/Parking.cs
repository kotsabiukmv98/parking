﻿using Parking.Core.Domain.Vehicles;
using System.Collections.Generic;

namespace Parking.Core.Domain
{
    /// <summary>
    /// Class that represents parking.
    /// </summary>
    public class Parking
    {
        /// <summary>
        /// Gets vehicle on parking
        /// </summary>
        public IList<Vehicle> Vehicles { get; } = new List<Vehicle>();

        /// <summary>
        /// Gets transactions for last minute
        /// </summary>
        public IList<Transaction> Transactions { get; } = new List<Transaction>();

        /// <summary>
        /// Gets parking balance
        /// </summary>
        public double Balance { get; set; } = Settings.ParkingBalance;

        /// <summary>
        /// Gets parking capacity.
        /// </summary>
        public int Capacity { get; set; } = Settings.ParkingCapacity;        
    }
}
