﻿using System;
using System.Text;
using static System.Console;
using static Parking.UI.ConsoleOutput;
using static Parking.UI.ConsoleInput;

namespace Parking.UI
{
    /// <summary>
    /// Responsive class for console UI
    /// </summary>
    public class ConsoleUI
    {
        private readonly IParkingActions _parkingActions;
        public ConsoleUI(IParkingActions parkingActions)
        {
            _parkingActions = parkingActions;
            OutputEncoding = Encoding.Unicode;
            InputEncoding = Encoding.Unicode;
        }

        public void PromptInput()
        {
            PrintAllActions();

            var input = ReadLine();
            var command = CheckCommandNumber(input);
            if (command != 0)
                ExecuteCommand(command);
        }

        private void ExecuteCommand(int commandNumber)
        {
            switch (commandNumber)
            {
                case 1:
                    GetParkingBalance();
                    return;
                case 2:
                    GetRevenueForLastMinut();
                    return;
                case 3:
                    GetParkingCapacity();
                    return;
                case 4:
                    GetTransactionForLastMinute();
                    return;
                case 5:
                    GetAllTransactions();
                    return;
                case 6:
                    GetAllVehicles();
                    return;
                case 7:
                    CreateVehicle();
                    return;
                case 8:
                    RemoveVehicle();
                    return;
                case 9:
                    RefillVehicleBalance();
                    return;
                case 10:
                    FinishWork();
                    return;
                default:
                    return;
            }
        }

        private void FinishWork()
        {
            Environment.Exit(0);
        }

        private void RefillVehicleBalance()
        {
            int vehicleId = PromptInt("Введіть ідентифікатор транспортного засобу, будь ласка.");
            while (!_parkingActions.HasVehicle(vehicleId))
            {
                DangerMessage($"Немає транспортного засобу з ідентифікатором \"{vehicleId}\"!");
                
                if (!ConfirmPrompt("Спробувати ще раз?"))
                    return;
                vehicleId = PromptInt("Введіть ідентифікатор транспортного засобу, будь ласка.");
            }

            double amount = PromptNonNegativeDouble("Введіть суму поповнення:");
            if (!ConfirmPrompt($"Поповнити транспортний засіб з ідентифікатором {vehicleId} на суму {amount}?"))
                return;

            _parkingActions.RefillVehicle(vehicleId, amount);
            SuccessMessage($"\nБаланс транспортного засобу було успішно поповнено)\n");
        }

        private void RemoveVehicle()
        {
            int vehicleId = PromptInt("Введіть ідентифікатор транспортного засобу, будь ласка.");
            while (!_parkingActions.HasVehicle(vehicleId))
            {
                DangerMessage($"Немає транспортного засобу з ідентифікатором \"{vehicleId}\"!");

                if (!ConfirmPrompt("Спробувати ще раз?"))
                    return;
                vehicleId = PromptInt("Введіть ідентифікатор транспортного засобу, будь ласка.");
            }

            if (!ConfirmPrompt($"Видалити транспортний засіб з ідентифікатором {vehicleId}?"))
                return;
            if (_parkingActions.RemoveVehicle(vehicleId))
                SuccessMessage($"\nТранспортний засіб було успішно видалено)\n");
            else
                DangerMessage("У транспортного засоба заборгованість!");

        }

        private void CreateVehicle()
        {
            if (_parkingActions.IsParkingFull())
            {
                DangerMessage("Вибачте, але парковка заповнена!");
                return;
            }

            var (vehicleType, balance) = PromptVehicle();
            if (!ConfirmPrompt($"Створити транспортний засіб \"{GetVehicleType(vehicleType)}\" з балансом {balance}?"))
                return;

            _parkingActions.AddVehicle(vehicleType, balance);
            SuccessMessage($"\n{GetVehicleType(vehicleType)} було додано на парковку з ідентифікатором {balance}.\n");
        }

        private string GetVehicleType(int vehicleType)
        {
            switch (vehicleType)
            {
                case 1: return "Легкове авто";
                case 2: return "Вантажний автомобіль";
                case 3: return "Автобус";
                case 4: return "Мотоцикл";
                default: return "Транспортний засіб";
            };
        }

        private (int, double) PromptVehicle()
        {
            var vehicleType = 0;
            while (vehicleType < 1 || vehicleType > 4)
            {
                PrintAllTypesOfVehicle();
                var input = ReadLine();
                
                if (!int.TryParse(input, out vehicleType))
                    DangerMessage("Некоректно введено номер дії. Переконайтесь, будь ласка, що ви ввели число");
                else if (vehicleType < 1 || vehicleType > 4)
                    DangerMessage($"Некоректно введено номер дії. Не існує дії з номером \"{vehicleType}\"");
            }
            double balance = PromptNonNegativeDouble("Введіть баланс транспортного засобу:");
            
            return (vehicleType, balance);
        }

        private void GetAllTransactions()
        {
            var transactions = _parkingActions.GetLoggedTransactions();
            PrintTransactionsHeader(transactions.Count);

            foreach (var transaction in transactions)
                WriteLine(transaction);
        }

        private void GetAllVehicles()
        {
            if (_parkingActions.GetVehiclesCount() == 0)
            {
                WriteLine("На парковці немає транспортних засобів.");
                return;
            }

            Write("\nВсього транспортних засобів на парковці: ");
            SuccessMessage($"{_parkingActions.GetVehiclesCount()}\n");            
            SuccessMessage($"{"Iдентифікатор", -20}{"Тип", -25}{"Баланс",-20}{"Дата і час створення",-30}");

            var vehicles = _parkingActions.GetVehicles();
            foreach (var vehicle in vehicles)
            {
                WriteLine(vehicle);
            }
            WriteLine();
        }

        private void GetTransactionForLastMinute()
        {
            PrintTransactionsHeader(_parkingActions.GetTransactionsCount());

            var transactions = _parkingActions.GetTransactions();
            foreach (var transaction in transactions)
            {
                WriteLine(transaction);
                //WriteLine($"{transaction.VehicleId,-20}{transaction.Amount,-25}{transaction.DateTime.ToString(),-30}");
            }

            WriteLine();
        }

        private void GetParkingCapacity()
        {
            Write("Всього мість на парковці: \t");
            SuccessMessage($"{_parkingActions.GetCapacity()}");
            Write("Кількість зайнятих місць: \t");
            SuccessMessage($"{_parkingActions.GetVehiclesCount()}");
            Write("Кількість вільних місць: \t");
            SuccessMessage($"{_parkingActions.GetCapacity() - _parkingActions.GetVehiclesCount()}");
        }

        private void GetRevenueForLastMinut()
        {
            Write("За останню хвилину зароблено: ");
            //SuccessMessage($"{Instance().GetRevenueForLastMinute()}");
            SuccessMessage($"{_parkingActions.GetRevenueForLastMinute()}");
        }

        private void GetParkingBalance()
        {
            Write("Баланс парковки: ");
            //SuccessMessage($"{Instance().Balance}");
            SuccessMessage($"{_parkingActions.GetBalance()}");

        }
    }  
}
