﻿using static System.Console;
using static Parking.UI.ConsoleOutput;

namespace Parking.UI
{
    /// <summary>
    /// Helper class for console input
    /// </summary>
    internal static class ConsoleInput
    {
        /// <summary>
        /// Gets INT number
        /// </summary>
        /// <param name="promptMessage">Prompt message</param>
        /// <returns></returns>
        public static int PromptInt(string promptMessage)
        {
            int promptNumber;
            string input;
            WriteLine(promptMessage);
            input = ReadLine();
            while (!int.TryParse(input, out promptNumber))
            {
                DangerMessage("Некоректний ввід! Необхідно ввсести ціле число!");
                input = ReadLine();
            }
            return promptNumber;
        }

        /// <summary>
        /// Gets non negative double number
        /// </summary>
        /// <param name="promptMessage">Prompt message</param>
        /// <returns></returns>
        public static double PromptNonNegativeDouble(string promptMessage)
        {
            double promptNumber;
            string input;
            WriteLine(promptMessage);
            input = ReadLine();
            while (!double.TryParse(input, out promptNumber) || promptNumber < 0)
            {
                DangerMessage("Некоректний ввід! Необхідно ввсести невід'ємне число!");
                input = ReadLine();
            }
            return promptNumber;
        }

        /// <summary>
        /// Confirm or not confirm some statement.
        /// </summary>
        /// <param name="question">Question for confirmation</param>
        /// <returns>True if user confirm, otherwise false</returns>
        public static bool ConfirmPrompt(string question)
        {
            string input = "";
            while (input != "ні" && input != "н" && input != "так" && input != "т")
            {
                WriteLine(question + " Введіть Так/Т або Ні/Н.");
                input = ReadLine().ToLower();
            }
            if (input == "ні" || input == "н")
                return false;
            return true;
        }
    }
}
