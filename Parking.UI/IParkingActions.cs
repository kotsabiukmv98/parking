﻿using System.Collections;
using System.Collections.Generic;

namespace Parking.UI
{
    public interface IParkingActions
    {
        double GetBalance();
        double GetRevenueForLastMinute();
        int GetCapacity();
        int GetVehiclesCount();
        int GetTransactionsCount();
        IList<string> GetTransactions();
        bool HasVehicle(int vehicleId);
        bool IsParkingFull();
        void AddVehicle(int vehicleType, double balance);
        IList<string> GetLoggedTransactions();
        IList<string> GetVehicles();
        bool RemoveVehicle(int vehicleId);
        void RefillVehicle(int vehicleId, double amount);
    }
}