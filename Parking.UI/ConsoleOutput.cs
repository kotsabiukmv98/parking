﻿using System;
using static System.Console;

namespace Parking.UI
{
    /// <summary>
    /// Helper class for console output
    /// </summary>
    internal static class ConsoleOutput
    {
        /// <summary>
        /// Checks whether input is valid command or not
        /// </summary>
        /// <param name="input">User input</param>
        /// <returns>Number of command if input is valid otherwise - zero</returns>
        public static int CheckCommandNumber(string input)
        {
            if (!int.TryParse(input, out var commandNumber))
            {
                DangerMessage("Некоректно введено номер дії. Переконайтесь, будь ласка, що ви ввели число");
                return 0;
            }
            else if (commandNumber < 1 || commandNumber > 10)
            {
                DangerMessage($"Некоректно введено номер дії. Не існує дії з номером \"{commandNumber}\"");
                return 0;
            }
            return commandNumber;
        }

        /// <summary>
        /// Prints all types of vehicle
        /// </summary>
        public static void PrintAllTypesOfVehicle()
        {
            SuccessMessage("Виберіть тип транспортного засобу:");
            WriteLine("\t1. Створити легкове авто.");
            WriteLine("\t2. Створити вантажний автомобіль.");
            WriteLine("\t3. Створити автобус.");
            WriteLine("\t4. Створити мотоцикл.");
            SuccessMessage("Для вибору введіть ТІЛЬКИ номер дії і натисніть \"Enter\"");
        }

        /// <summary>
        /// Prints all actions
        /// </summary>
        public static void PrintAllActions()
        {
            SuccessMessage("Виберіть дію, будь ласка:");
            WriteLine("\t1. Дізнатися поточний баланс парковки.");
            WriteLine("\t2. Дізнатися суму зароблених коштів за останню хвилину.");
            WriteLine("\t3. Дізнатися кількість вільних/зайнятих місць на парковці.");
            WriteLine("\t4. Вивести на екран усі транзакції парковки за останню хвилину.");
            WriteLine("\t5. Вивести усю історію транзакцій(зчитавши дані з файлу Transactions.log).");
            WriteLine("\t6. Вивести на екран список усіх транспортних засобів.");
            WriteLine("\t7. Створити/поставити транспортний засіб на парковку.");
            WriteLine("\t8. Видалити/забрати транспортний засіб з парковки.");
            WriteLine("\t9. Поповнити баланс конкретного транспортного засобу.");
            WriteLine("\t10. Завершити роботу.");
            SuccessMessage("Для вибору введіть ТІЛЬКИ номер дії і натисніть \"Enter\"");
        }

        /// <summary>
        /// Prints count of transactions and header of a table if there is at least one transaction.
        /// </summary>
        /// <param name="transactionsCount">Count of transaction</param>
        public static void PrintTransactionsHeader(int transactionsCount)
        {
            if (transactionsCount == 0)
            {
                WriteLine("Немає транзакцій.");
                return;
            }

            Write("\nВсього транзакцій: ");
            SuccessMessage($"{transactionsCount}\n");
            SuccessMessage($"{"Iдентифікатор ТЗ",-20}{"Кількість грошей",-25}{"Дата і час створення",-30}");
        }

        /// <summary>
        /// Outputs dangered message
        /// </summary>
        /// <param name="errorMessage">Message for output</param>
        public static void DangerMessage(string errorMessage)
        {
            ChangeConsoleColor(ConsoleColor.Red, ConsoleColor.White);
            WriteLine(errorMessage);
            ChangeConsoleColor();
        }

        /// <summary>
        /// Outputs successful message
        /// </summary>
        /// <param name="errorMessage">Message for output</param>
        public static void SuccessMessage(string errorMessage)
        {
            ChangeConsoleColor(ConsoleColor.Green);
            WriteLine(errorMessage);
            ChangeConsoleColor();
        }

        /// <summary>
        /// Changes console foreground and background colors
        /// </summary>
        /// <param name="foreground">Console foreground color</param>
        /// <param name="background">Console background color</param>
        public static void ChangeConsoleColor(ConsoleColor foreground = ConsoleColor.White, ConsoleColor background = ConsoleColor.Black)
        {
            ForegroundColor = foreground;
            BackgroundColor = background;
        }
    }
}
