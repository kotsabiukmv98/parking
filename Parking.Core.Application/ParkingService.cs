﻿using Parking.Core.Application.Interfaces;
using Parking.Core.Domain;
using Parking.Core.Domain.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Parking.Core.Application
{
    /// <summary>
    /// Class for interaction with parking
    /// </summary>
    public class ParkingService
    {
        private readonly Domain.Parking _parking = new Domain.Parking();
        private static ParkingService _instance;
        private readonly ITransactionsLogging _logging = new FileLogging(@"C:\temp\Transactions.log");


        private ParkingService()
        {
            _logging.SubscribeForSaving(_parking);
        }

        /// <summary>
        /// Gets singleton instance of parking service
        /// </summary>
        /// <returns>Parking instance</returns>
        public static ParkingService Instance() => _instance ?? (_instance = new ParkingService());

        /// <summary>
        /// Tells whether parking is full or not.
        /// </summary>
        /// <returns>True if parking is full, otherwise - false</returns>
        public bool IsParkingFull() => _parking.Vehicles.Count == _parking.Capacity;
        
        /// <summary>
        /// Tells whether a vehicle with some ID is on parking or not
        /// </summary>
        /// <param name="vehicleId">Vehicle ID</param>
        /// <returns>True if vehicle is on parking, otherwise - false</returns>
        public bool HasVehicle(int vehicleId) => _parking.Vehicles.Any(v => v.Id == vehicleId);

        /// <summary>
        /// Gets count of transactions
        /// </summary>
        public int TransactionsCount => _parking.Transactions.Count;

        /// <summary>
        /// Gets count of vehicles on parking
        /// </summary>
        public int VehiclesCount => _parking.Vehicles.Count;

        /// <summary>
        /// Gets logged transactions from file
        /// </summary>
        /// <returns>List of logged transactions</returns>
        public List<string> GetLoggedTransactions() => _logging.GetTransactions();

        public IList<Vehicle> Vehicles => _parking.Vehicles;

        public double Balance => _parking.Balance;

        public int Capacity => _parking.Capacity;

        public IList<Transaction> Transactions => _parking.Transactions;


        /// <summary>
        /// Removes vehicle from parking if it hasn't debt
        /// </summary>
        /// <param name="vehicleId">Vehicle ID</param>
        /// <returns>True if vehicle was removed, otherwise - false</returns>
        public bool RemoveVehicle(int vehicleId)
        {
            var vehicle = _parking.Vehicles.FirstOrDefault(v => v.Id == vehicleId);
            if (vehicle == null)
                return false;
 
            if (vehicle.Balance < 0)
                return false;
            vehicle.Payment.Stop();
            _parking.Vehicles.Remove(vehicle);

            return true;
        }

        /// <summary>
        /// Gets revenue for last minute
        /// </summary>
        /// <returns>Revenue for last minute</returns>
        public double GetRevenueForLastMinute()
        {
            double amount = 0;
            foreach (var transaction in _parking.Transactions)
                amount += transaction.Amount;
            return amount;
        }

        /// <summary>
        /// Puts vehicle on parking
        /// </summary>
        /// <param name="vehicle">Vehicle</param>
        public void AddVehicle(Vehicle vehicle)
        {
            if (IsParkingFull())
                return;

            _parking.Vehicles.Add(vehicle);
            vehicle.Payment.Elapsed += (s, a) =>
            {
                var transaction = new Transaction()
                {
                    VehicleId = vehicle.Id,
                    Amount = vehicle.Balance > vehicle.Rate ? vehicle.Rate : vehicle.Rate * Settings.FineCoefficient,
                    DateTime = DateTime.Now
                };
                _parking.Transactions.Add(transaction);
                vehicle.Pay(transaction.Amount);
                _parking.Balance += transaction.Amount;

            };
            vehicle.Payment.Start();
        }

        /// <summary>
        /// Refill vehicle's balance
        /// </summary>
        /// <param name="vehicleId">Vehicle ID</param>
        /// <param name="amount">Amount of money</param>
        public void RefillVehicle(int vehicleId, double amount)
        {
            if (amount > 0)
               _parking.Vehicles.FirstOrDefault(v => v.Id == vehicleId)?.ReplenishBalance(amount);
        }
    }
}
