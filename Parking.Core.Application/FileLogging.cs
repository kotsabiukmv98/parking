﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Timers;
using System.Text;
using System.Threading.Tasks;
using Parking.Core.Application.Interfaces;

namespace Parking.Core.Application
{
    class FileLogging : ITransactionsLogging
    {
        private readonly Timer _logTransactions = new Timer(60 * 1000);
        private readonly string _fileName;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="fileName">File name for saving transactions</param>
        public FileLogging(string fileName = "Transactions.log")
        {
            _fileName = fileName;
        }

        /// <inheritdoc />
        public void SubscribeForSaving(Domain.Parking parking)
        {
            if (File.Exists(_fileName))
                File.Delete(_fileName);

            using (StreamWriter sw = File.CreateText(_fileName))
            {
            }

            _logTransactions.Elapsed += (s, a) =>
            {
                foreach (var transaction in parking.Transactions)
                {
                    string log = $"{transaction.VehicleId,-20}{transaction.Amount,-25}{transaction.DateTime.ToString(),-30}";
                    using (var sw = File.AppendText(_fileName))
                    {
                        sw.WriteLine(log);
                    }
                    double delay = 60 * 1000 - (DateTime.Now - transaction.DateTime).TotalMilliseconds;
                    Task.Run(async delegate
                    {
                        await Task.Delay(TimeSpan.FromMilliseconds(delay));
                        parking.Transactions.Remove(transaction);
                    });
                }
            };
            _logTransactions.Start();
        }

        /// <inheritdoc />
        public List<string> GetTransactions()
        {
            var transactions = new List<string>();
            using (StreamReader sr = File.OpenText(_fileName))
            {
                string transaction = "";
                while ((transaction = sr.ReadLine()) != null)
                {
                    transactions.Add(transaction);
                }
            }
            return transactions;
        }
    }
}
