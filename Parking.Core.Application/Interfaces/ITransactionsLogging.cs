﻿using System.Collections.Generic;

namespace Parking.Core.Application.Interfaces
{
    /// <summary>
    /// Interface for logging transactions
    /// </summary>
    interface ITransactionsLogging
    {
        /// <summary>
        /// Subscribing for saving transactions
        /// </summary>
        /// <param name="parking"></param>
        void SubscribeForSaving(Domain.Parking parking);

        /// <summary>
        /// Gets logged transactions
        /// </summary>
        /// <returns>List of logged transactions</returns>
        List<string> GetTransactions();
    }
}
